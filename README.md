# cvm.py
`cvm.py` is a small library which can be used to create a CollabVM server. As such, it will not work as a replacement
for the CollabVM server out of the box. In fact, it has extremely bare support for basic things.

For now cvm.py does not target CollabVM Server v3.x. This may change in the future with the release of CollabVM Server
v3.x.

## How to use?
You can install cvm.py like this:
```sh
pip install git+https://gitlab.com/omaemae/cvm.py.git
```

You can then use it in your code like this:
```py
import cvmpy

class MyVM(cvmpy.VM):
    pass
```

## Contributions
Contributions to extend the spec to match CollabVM Server v1.x, fix bugs and clean up the codebase are always welcome.
