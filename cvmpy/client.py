import asyncio
import logging
import random
from typing import Optional
from aiohttp import web
from cvmpy import guacamole


class WebSocketClient:
    def __init__(self, websocket: web.WebSocketResponse, request: web.Request, server):
        self._ws = websocket
        self._ping_task: Optional[asyncio.Task] = None
        self._server = server
        self.ip = request.remote
        self.nickname = f"cvmpy_guest{random.randint(0, 100000)}"
        self.current_vm = None
        self.rank = 0
        """
        Possible ranks:

        - 0: Guest
        - 1: Registered (unused)
        - 2: Admin
        - 3: Moderator
        """
        self.logger = logging.getLogger(f"cvmpy.WebSocketClient ({self.ip})")

    async def _ping_task_func(self):
        while not self._ws.closed:
            await asyncio.sleep(2)
            await self.send(guacamole.Command("nop"))

    async def send(self, *commands: guacamole.Command):
        if self._ws.closed:
            await self.ghost_client()
        for command in commands:
            try:
                await self._ws.send_str(str(command))
            except (ConnectionResetError, BrokenPipeError):
                await self.ghost_client()
                return

    async def destroy(self):
        self._ping_task.cancel()

    async def initialize(self):
        self._ping_task = asyncio.create_task(self._ping_task_func())

    async def close(self):
        await self._ws.close()

    async def ghost_client(self):
        self.logger.info(f"Ghost client detected, forcefully removing client.")
        await self.destroy()
        await self._server.remove_client(self)
