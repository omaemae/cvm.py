from typing import Union


class Command:
    def __init__(self, command: str, *arguments: Union[str | int]):
        self.command = command
        self.arguments = [str(argument) for argument in arguments]

    def __str__(self):
        args = [f"{len(a)}.{a}" for a in self.arguments]
        return ",".join([f"{len(self.command)}.{self.command}", *args]) + ";"


class InvalidGuacamoleInstructionException(BaseException):
    pass


def parse_guac(data: str):
    pos = -1
    sections = []

    while True:
        section_len = data.find(".", pos + 1)
        if section_len == -1:
            break

        try:
            pos = int(data[pos + 1:section_len]) + section_len + 1
        except ValueError:
            raise InvalidGuacamoleInstructionException(f"length is not a number")

        if pos > len(data):
            raise InvalidGuacamoleInstructionException(f"data is longer than received")

        sections.append(data[section_len + 1: pos])

        sep = data[pos:pos + 1]

        if sep == ",":
            continue
        elif sep == ";":
            break
        else:
            raise InvalidGuacamoleInstructionException(f"the separator ({sep}) is invalid")

    return sections


def get_command_from_str(data: str):
    arguments = parse_guac(data)
    command = arguments.pop(0)
    return Command(command, *arguments)
