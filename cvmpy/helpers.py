def flatten_list(input: list[iter]) -> list:
    return [nested_item for item in input for nested_item in item]


def list_without_none(input: list) -> list:
    return [e for e in input if e is not None]
