import asyncio
import logging
import random
import aiohttp
from aiohttp import web
from . import guacamole
from .client import WebSocketClient
from .vm import VM


class WebSocketServer:
    def __init__(self, vms: list[VM]):
        self.logger = logging.getLogger("cvmpy.WebSocketServer")
        self.clients: set[WebSocketClient] = set()
        self.vms = vms
        self.name_to_vm: dict[str, VM] = {vm.name: vm for vm in self.vms}
        self.blacklisted_nicknames = ["jjjj"]

    async def handle_rename(self, client: WebSocketClient, command: guacamole.Command):
        old_nickname = client.nickname
        client_nicknames = [other_client.nickname for other_client in self.clients]
        client.nickname = command.arguments[0].replace("<", "&lt;").replace(">", "&gt;").strip()[:32] \
            if len(command.arguments) != 0 else f"cvmpy_guest{random.randint(0, 100000)}"
        for nickname in self.blacklisted_nicknames:
            if nickname.lower() in client.nickname.lower():
                self.logger.info(f"{client.ip} tried logging in as {client.nickname}, however that username is "
                                 f"blacklisted. Killing the connection.")
                client.nickname = old_nickname
                await client.close()
                return
        if client.nickname in client_nicknames:
            self.logger.info(f"{client.ip} tried logging in as {client.nickname}, however someone else is already "
                             f"logged in as that user.")
            client.nickname = old_nickname
            await client.send(
                guacamole.Command("rename", 0, 1, client.nickname, 0)
            )
            return

        def get_command(other_client):
            if other_client == client:
                return guacamole.Command("rename", 0, 0, client.nickname, 0)
            return guacamole.Command("rename", 1, old_nickname, client.nickname, 0)

        await asyncio.gather(*[
             other_client.send(get_command(other_client)) for other_client in self.clients
        ])
        self.logger.info(f"{client.ip} logged in as {client.nickname}")

    async def handle_list(self, client: WebSocketClient):
        servers_as_guacamole_arguments = [
            vm.as_guacamole_arguments() for vm in self.vms
        ]
        flattened_servers = [nested_item for item in servers_as_guacamole_arguments for nested_item in item]
        await client.send(guacamole.Command("list", *flattened_servers))

    async def handle_websocket_message(self, client: WebSocketClient, message: str):
        command = guacamole.get_command_from_str(message)
        match command.command:
            case "rename":
                await self.handle_rename(client, command)
            case "list":
                await self.handle_list(client)
            case "connect":
                if len(command.arguments) == 0:
                    return
                name = command.arguments[0]
                if name not in self.name_to_vm:
                    return
                await self.name_to_vm[name].join(client)
            case "turn":
                if client.current_vm is None:
                    return
                if len(command.arguments) == 0:
                    await client.current_vm.take_turn(client)
                else:
                    await client.current_vm.cancel_turn(client)
            case "mouse":
                if client.current_vm is None:
                    return
                await client.current_vm.handle_mouse_move(client, command)
            case "key":
                if client.current_vm is None:
                    return
                await client.current_vm.handle_key_press(client, command)
            case "chat":
                if client.current_vm is None:
                    return
                await client.current_vm.send_chat_message(client, command.arguments[0])
            case "vote":
                if client.current_vm is None:
                    return
                vote = bool(int(command.arguments[0]))
                await client.current_vm.handle_vote(client, vote)
            # afaik only used by User VM, implementing it anyway
            case "disconnect":
                await client.close()
            case "nop":
                return
            case _:
                await client.send(guacamole.Command("cvmpy_error", "unknown command", command.command))

    async def handle_aiohttp_websocket(self, request: web.Request):
        ws = web.WebSocketResponse(protocols=["guacamole"], compress=False)
        try:
            await ws.prepare(request)
        except ConnectionResetError as exc:
            self.logger.warning(f"Client most likely quit before we sent a response. {str(exc)}")
            return

        client = WebSocketClient(ws, request, self)
        await client.initialize()

        self.logger.info(f"Received connection from {client.ip}.")
        self.clients.add(client)

        try:
            async for msg in ws:
                msg: aiohttp.WSMessage
                if msg.type == aiohttp.WSMsgType.TEXT:
                    await self.handle_websocket_message(client, msg.data)
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    self.logger.error("Connection closed with exception: %s", ws.exception())
        finally:
            self.logger.info(f"Connection from {client.ip} was closed.")
            await self.remove_client(client)

        return ws

    async def remove_client(self, client: WebSocketClient):
        if client.current_vm:
            await client.current_vm.leave(client)
        await client.destroy()
        if client in self.clients:
            self.clients.remove(client)
