from aiohttp import web
import cvmpy
import logging
from cvmpy.examples import DrawVM

logging.basicConfig(level=logging.INFO, format="[%(asctime)s | %(levelname)s] [%(name)s] %(message)s")

server = cvmpy.WebSocketServer([DrawVM()])

app = web.Application()
app.add_routes([web.get('/', server.handle_aiohttp_websocket)])

if __name__ == "__main__":
    web.run_app(app, port=6262)
