import os
from PIL import Image
from cvmpy import VM, MouseData


class DrawVM(VM):
    def __init__(self):
        super().__init__("cvmpy_draw", "<b><i>Draw!</i></b><br><small>a <b>cvm.py</b> demo</small>", (800, 600))
        self.welcome_message = "Welcome to this <a href=\"https://gitlab.com/omaemae/cvm.py\" target=\"_blank\">" \
                               "cvm.py</a> demo called <i>\"Draw!\"</i>. Read the instructions on screen and go wild!"

        self.banner = Image.open(os.path.join(os.path.dirname(__file__), "../assets/draw_banner.png"))
        self.banner_start = self.image.size[1] - 64
        self.image.paste(self.banner, (0, self.banner_start))
        self.colors = [
            (255, 255, 255),
            (255, 0, 0),
            (0, 255, 0),
            (0, 0, 255),
            (255, 255, 0),
            (0, 255, 255),
            (255, 0, 255),
            (0, 0, 0),
            (128, 0, 0),
            (0, 128, 0),
            (0, 0, 128),
            (128, 128, 0),
            (0, 128, 128),
            (128, 0, 128),
        ]
        self.current_color = 0
        self.color_box_start = (800 - (round(len(self.colors) / 2) * 26), 600 - 64 + 9)

        self.draw_color_boxes()

        self.has_turns = True
        self.has_votes = True
        self.voting_time = 10
        self.turn_time = 40

        self._old_x = 0
        self._old_y = 0

    def draw_color_boxes(self):
        row_size = 22
        row_padding = 2
        color_size = 16 + 2  # (???)
        border_1_color = (64, 64, 64)
        border_2_color = (128, 128, 128)
        selected_border_1_color = (0, 0, 0)
        selected_border_2_color = (64, 64, 64)
        colors_per_row = round(len(self.colors) / 2)
        color_rows = [self.colors[:colors_per_row], self.colors[colors_per_row:]]
        for i in range(len(color_rows)):
            row_start = (self.color_box_start[0], self.color_box_start[1] + (row_size * i + row_padding * i))
            for j in range(len(color_rows[i])):
                color_index = i * colors_per_row + j
                selected = self.current_color == color_index
                color_start = (row_start[0] + ((color_size + 4) * j + row_padding * j), row_start[1])
                self.draw.rectangle((color_start[0], color_start[1],
                                     color_start[0] + color_size + 3, color_start[1] + color_size + 3),
                                    fill=selected_border_1_color if selected else border_1_color)
                self.draw.rectangle((color_start[0] + 1, color_start[1] + 1,
                                     color_start[0] + color_size + 2, color_start[1] + color_size + 2),
                                    fill=selected_border_2_color if selected else border_2_color)
                self.draw.rectangle((color_start[0] + 3, color_start[1] + 3,
                                     color_start[0] + color_size, color_start[1] + color_size),
                                    fill=color_rows[i][j])

    async def draw_line_segment(self, x: int, y: int):
        if self._old_x == 0 or y >= self.banner_start or self._old_y >= self.banner_start:
            return
        self.draw.line((self._old_x, self._old_y, x, y), fill=self.colors[self.current_color])

        pos_1 = (
            (x if x < self._old_x else self._old_x),
            (y if y < self._old_y else self._old_y)
        )

        pos_2 = (
            (x if x > self._old_x else self._old_x) + 1,
            (y if y > self._old_y else self._old_y) + 1
        )

        wh = (
            pos_2[0] - pos_1[0],
            pos_2[1] - pos_1[1],
        )

        await self.send_updated_area(pos_1, wh)

    async def on_reset(self):
        self.draw.rectangle((0, 0, self.image.size[0], self.banner_start), fill=(0, 0, 0))
        await self.send_updated_area((0, 0), (self.image.size[0], self.banner_start))

    async def handle_box_press(self, x: int, y: int):
        row_size = 22
        row_padding = 2
        color_size = 16 + 2  # (???)
        colors_per_row = round(len(self.colors) / 2)
        color_rows = [self.colors[:colors_per_row], self.colors[colors_per_row:]]
        for i in range(len(color_rows)):
            row_start = (self.color_box_start[0], self.color_box_start[1] + (row_size * i + row_padding * i))
            for j in range(len(color_rows[i])):
                color_index = i * colors_per_row + j
                color_start = (row_start[0] + ((color_size + 4) * j + row_padding * j), row_start[1])
                if ((color_start[0] < x < color_start[0] + color_size + 3)
                        and (color_start[1] < y < color_start[1] + color_size + 3)):
                    self.current_color = color_index
                    self.draw_color_boxes()
                    await self.send_updated_area(self.color_box_start, (166, 46))

    async def on_mouse_move(self, x: int, y: int, data: MouseData):
        if not (data.left or data.right):
            self._old_x = 0
            self._old_y = 0
        if data.left and (y < self.banner_start):
            await self.draw_line_segment(x, y)
            self._old_x = x
            self._old_y = y
        elif data.left:
            await self.handle_box_press(x, y)
