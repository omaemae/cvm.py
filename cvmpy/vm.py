import asyncio
import base64
import io
import logging
import time
from asyncio import Task
from collections import deque
from typing import Optional
from PIL import Image, ImageDraw

from cvmpy import guacamole
from cvmpy.client import WebSocketClient
from cvmpy.helpers import flatten_list, list_without_none


class MouseData:
    def __init__(self, data: int):
        self.left = data & 1 != 0
        self.middle = data & 2 != 0
        self.right = data & 4 != 0
        self.scroll_up = data & 8 != 0
        self.scroll_down = data & 16 != 0


class VM:
    def __init__(self, name: str, display_name: str, resolution: tuple[int, int]):
        self.name = name
        self.display_name = display_name
        self.thumbnail_data: Optional[bytes] = None
        self.image = Image.new("RGB", resolution, color=(0, 0, 0))
        self.draw = ImageDraw.Draw(self.image, "RGBA")
        self.clients: set[WebSocketClient] = set()

        self.logger = logging.getLogger(f"cvmpy.VM ({self.name})")

        self.welcome_message = ""
        self.has_turns = False
        self.has_votes = False
        self.voting_time = 60
        self.turn_time = 20
        self.cooldown_max_messages = 4
        self.cooldown_seconds = 2
        self.chat_history_length = 10

        self._current_turn: Optional[WebSocketClient] = None
        self._queue_turn: deque[WebSocketClient] = deque()
        self._turn_end: int = 0
        self._turn_sleep_task: Optional[Task] = None
        self._chat: list[tuple[str, str]] = []
        self._yes_votes: set[WebSocketClient] = set()
        self._no_votes: set[WebSocketClient] = set()
        self._vote_end = 0
        self._active_vote = False
        self._client_cooldown: dict[WebSocketClient, list] = {}

    # Events
    async def on_message(self, sender: WebSocketClient, message: str):
        pass

    async def on_mouse_move(self, x: int, y: int, data: MouseData):
        pass

    async def on_reset(self):
        pass

    async def on_key(self, keycode: int, down: bool):
        pass

    async def _broadcast_command(self, *commands: guacamole.Command):
        await asyncio.gather(*[
            client.send(*commands) for client in self.clients
        ])

    async def handle_mouse_move(self, client: WebSocketClient, command: guacamole.Command):
        if client != self._current_turn:
            return
        await self.on_mouse_move(
            int(command.arguments[0]),
            int(command.arguments[1]),
            MouseData(int(command.arguments[2]))
        )

    async def handle_key_press(self, client: WebSocketClient, command: guacamole.Command):
        if client != self._current_turn:
            return
        await self.on_key(
            int(command.arguments[0]),
            bool(int(command.arguments[1]))
        )

    async def send_updated_area(self, xy: tuple[int, int], wh: tuple[int, int]):
        updated_area = self.image.crop((xy[0], xy[1], xy[0] + wh[0], xy[1] + wh[1]))
        b64 = self._get_image_base64(updated_area)
        await self._broadcast_command(
            guacamole.Command("png", 14, 0, *xy, b64),
            guacamole.Command("sync", round(time.time() * 1000))
        )

    @staticmethod
    def _get_image_base64(image: Image.Image):
        buffer = io.BytesIO()
        image.save(buffer, format="PNG")
        data = buffer.getvalue()
        return base64.b64encode(data).decode()

    def as_guacamole_arguments(self):
        thumb = base64.b64encode(self.thumbnail_data).decode() if self.thumbnail_data \
            else self._get_image_base64(self.image)
        return [self.name, self.display_name, thumb]

    async def _send_chat_message(self, nickname: str, message: str):
        self._chat.append((nickname, message))
        if len(self._chat) > self.chat_history_length:
            self._chat.pop(0)
        await self._broadcast_command(guacamole.Command("chat", nickname, message))

    async def send_chat_message(self, client: WebSocketClient, message: str):
        message = message.replace("<", "&lt;").replace(">", "&gt;")[:100]
        if client not in self._client_cooldown:
            self._client_cooldown.update({client: [1, time.time() + self.cooldown_seconds]})
        else:
            self._client_cooldown[client][0] += 1
            if (self._client_cooldown[client][1] > time.time()
                    and self._client_cooldown[client][0] > self.cooldown_max_messages):
                await client.send(guacamole.Command("chat", "", "Calm down, you're sending too many messages!"))
                return
            elif self._client_cooldown[client][1] < time.time():
                self._client_cooldown[client][0] = 1
                self._client_cooldown[client][1] = time.time() + 2
        await self._send_chat_message(client.nickname, message)
        await self.on_message(client, message)
        self.logger.info(f"CHAT: {client.ip} ({client.nickname}) > {message}")

    def _get_vote_status(self, updated: bool = True):
        return guacamole.Command(
            "vote", int(updated), round((self._vote_end - time.time()) * 1000), len(self._yes_votes),
            len(self._no_votes)
        )

    async def _update_vote_status(self, updated: bool = True):
        cmd = self._get_vote_status(updated)
        await self._broadcast_command(cmd)

    async def _conclude_vote(self):
        cmd = guacamole.Command("vote", 2)
        for client in self.clients:
            await client.send(cmd)

    async def _vote_task(self):
        await asyncio.sleep(self._vote_end - time.time() - 1)
        won = len(self._yes_votes) >= len(self._no_votes)
        total_votes = len(self._yes_votes) + len(self._no_votes)
        if total_votes != 0:
            percent = round(len(self._yes_votes) / total_votes * 100)
        else:
            percent = 0
        await self._conclude_vote()
        await self._send_chat_message("", f"The vote to reset the VM has {'won' if won else 'lost'}, with {percent}% "
                                          f"of votes in favor of the reset.")
        if won:
            await self.on_reset()
        self._active_vote = False

    async def handle_vote(self, client: WebSocketClient, yes: bool):
        if not self._active_vote:
            self._active_vote = True
            self._vote_end = time.time() + self.voting_time
            self._yes_votes = {client}
            self._no_votes = set()
            await self._update_vote_status(False)
            await self._send_chat_message("", f"<i>{client.nickname}</i> has started a vote to reset the VM.")
            asyncio.create_task(self._vote_task())
        else:
            if yes:
                if client in self._no_votes:
                    self._no_votes.remove(client)
                elif client in self._yes_votes:
                    return
                self._yes_votes.add(client)
            else:
                if client in self._yes_votes:
                    self._yes_votes.remove(client)
                elif client in self._no_votes:
                    return
                self._no_votes.add(client)
            await self._update_vote_status()
            await self._send_chat_message("", f"<i>{client.nickname}</i> voted {'yes' if yes else 'no'}.")

    async def join(self, client: WebSocketClient):
        self.logger.info(f"{client.ip} ({client.nickname}) joined")
        other_clients = self.clients.copy()
        for other_client in other_clients:
            await other_client.send(
                guacamole.Command("adduser", 1, client.nickname, client.rank)
            )
        client.current_vm = self
        self.clients.add(client)
        user_list = flatten_list([
            [other_client.nickname, other_client.rank] for other_client in other_clients
        ])
        await client.send(*list_without_none([
            guacamole.Command("connect", 1, int(self.has_turns), int(self.has_votes), 0),
            guacamole.Command("adduser", len(other_clients), *user_list) if len(other_clients) != 0 else None,
            guacamole.Command("chat", *flatten_list(self._chat)) if len(self._chat) != 0 else None,
            guacamole.Command("chat", "", self.welcome_message) if self.welcome_message else None,
            self._get_vote_status() if self._active_vote else None,
            self._get_turn_command(client) if self._current_turn is not None else None,
            guacamole.Command("size", 0, *self.image.size),
            guacamole.Command("png", 14, 0, 0, 0, self._get_image_base64(self.image)),
            guacamole.Command("sync", round(time.time() * 1000)),
        ]))

    def _get_turn_command(self, target_client: WebSocketClient) -> guacamole.Command:
        time_left = round((self._turn_end - time.time()) * 1000)
        queue_turn_as_users = [client.nickname for client in self._queue_turn]
        if self._current_turn is not None:
            if target_client in self._queue_turn:
                index = self._queue_turn.index(target_client)
                wait_left = (self.turn_time * index * 1000) + time_left
                return guacamole.Command("turn", time_left, 1 + len(self._queue_turn), self._current_turn.nickname,
                                         *queue_turn_as_users, wait_left)
            else:
                return guacamole.Command("turn", time_left, 1 + len(self._queue_turn), self._current_turn.nickname,
                                         *queue_turn_as_users)
        else:
            return guacamole.Command("turn", 0, 0)

    async def _update_turn_status(self):
        await asyncio.gather(*[
            client.send(self._get_turn_command(client)) for client in self.clients
        ])

    async def _new_turn_if_needed(self):
        if len(self._queue_turn) != 0 and self._current_turn is None:
            self._current_turn = self._queue_turn.popleft()
            self._turn_end = time.time() + self.turn_time
            self._turn_sleep_task = asyncio.create_task(self._end_turn_task())
        await self._update_turn_status()

    async def _end_turn_task(self):
        await asyncio.sleep(self._turn_end - time.time())
        self._current_turn = None
        await self._new_turn_if_needed()

    async def take_turn(self, client):
        if client == self._current_turn:
            return

        if client not in self._queue_turn:
            self._queue_turn.append(client)
            await self._new_turn_if_needed()

    async def cancel_turn(self, client):
        if client != self._current_turn or self._current_turn is None:
            return

        self._current_turn = None
        self._turn_sleep_task.cancel()
        await self._new_turn_if_needed()

    async def leave(self, client: WebSocketClient):
        if client not in self.clients:
            return
        self.logger.info(f"{client.ip} ({client.nickname}) left")
        self.clients.remove(client)
        await self._broadcast_command(guacamole.Command("remuser", 1, client.nickname))
        if client == self._current_turn:
            await self.cancel_turn(client)
        if client in self._queue_turn:
            self._queue_turn.remove(client)
            await self._update_turn_status()
        if client in self._yes_votes and self._active_vote:
            self._yes_votes.remove(client)
        if client in self._no_votes and self._active_vote:
            self._no_votes.remove(client)
        if self._active_vote:
            await self._update_vote_status()
