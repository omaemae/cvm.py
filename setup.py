from setuptools import setup, find_packages

setup(
    name="cvmpy",
    version="1.1.5",
    author="omame",
    author_email="me@omame.xyz",
    packages=find_packages(include=["cvmpy", "cvmpy.*"]),
    package_data={"cvmpy": ["assets/draw_banner.png"]},
    install_requires=[
        "aiohttp",
        "Pillow"
    ]
)
